import React, { Component } from "react";
import API from "../data/API";
import LinkStore from "../data/LinkStore";

import "./App.css";

let _getAppState = () => {
  return { links: LinkStore.getAll() };
};

class APP extends Component {
  constructor(props) {
    super(props);
    this.state = _getAppState();
  }

  componentDidMount() {
    API.fetchLinks();
    LinkStore.on("change", this.onChange);
  }

  componentWillUnmount() {
    LinkStore.removeListener("change", this.onChange);
  }

  onChange = () => {
    this.setState(_getAppState());
  };

  render() {
    let content = this.state.links.map(link => {
      return (
        <li key={link.id}>
          {link.title} | {link.cpu}{" "}
        </li>
      );
    });
    return (
      <div>
        <div>links</div>
        <ul>{content}</ul>
      </div>
    );
  }
}

export default APP;
