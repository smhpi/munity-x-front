import AppDispatcher from "./AppDispatcher";
import { ActionTypes } from "./Constants";

let Actions = {
  reciveLinks(links) {
    AppDispatcher.dispatch({
      actionType: ActionTypes.RECEIVE_LINKS,
      links
    });
  }
};

export default Actions;
