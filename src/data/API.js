import { get } from "jquery";
import Actions from "./Actions";

let API = {
  fetchLinks() {
    get("/api/links").done(res => {
      Actions.reciveLinks(res);
    });
  }
};

export default API;
